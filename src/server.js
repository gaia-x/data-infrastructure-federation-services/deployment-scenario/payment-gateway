require('dotenv').config();
const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const plugins = require('./plugins.js');
const Mailjet = require('node-mailjet');
const mailjet = new Mailjet({
    apiKey: process.env.MJ_APIKEY_PUBLIC,
    apiSecret: process.env.MJ_APIKEY_PRIVATE
});
const app = express();
const opn = require('opn');
const default_port = 5003;
app.use(express.json());
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.post('/payInvoice/:paymentMethod/:paymentSolution', async (req, res) => {
    try {
        // generate invoice
        const invoice = await plugins.pluginsExecute(req.params.paymentSolution, req.params.paymentMethod, req, req.body);
        invoice.paymentSolution = req.params.paymentSolution

        const request = mailjet
            .post('send', { version: 'v3.1' })
            .request({
                Messages: [
                    {
                        From: {
                            Email: `${process.env.MJ_SENDER_ADRESS}`,
                            Name: "Payment Gateway"
                        },
                        To: [
                            {
                                Email: `${req.body.invoice.debitorEmail}`,
                                Name: ""
                            }
                        ],
                        Subject: "Nouvelle facture!",
                        TextPart: "Cher client, vous venez de recevoir une nouvelle facture à payer",
                        HTMLPart: `<h3>Cher client, vous venez de recevoir une nouvelle facture à payer par ${req.params.paymentMethod}.</br> Veuillez cliquer sur ce lien pour effectuer son paiement <a href=${invoice.paymentLink}>${req.params.paymentSolution}</a></h3><br />`
                    }
                ]
            });

        await request;
        //await opn(invoice.paymentLink)
        res.status(200).json({ message: 'Invoices payment triggered successfully', paidInvoice: invoice });
    } catch (error) {
        console.error('Error triggering invoice payment:', error.message);
        res.status(500).json({ error: 'Error making Error triggering invoice payment' });
    }
})

app.listen(default_port, () => {
    console.log(`server started at port ${default_port}`);
    plugins.pluginsLoad(app).then(r =>  console.log(`plugins deployed successfully`));
});