const fs = require('fs');
const path = require('path');
const {logger} = require('./utils.js');

global.PLUGINS = new Map();

const pluginDirectory = path.join(__dirname, "/plugins/");

var pluginsLoad = function () {
    return new Promise((result, error) => {
    try {
        fs.readdirSync(pluginDirectory).forEach(service => {
            if (fs.statSync(pluginDirectory + service).isDirectory()) {
                fs.readdirSync(pluginDirectory + service).forEach(file => {
                    if (path.extname(file) === ".js") {
                        const plugin = require(pluginDirectory + service + '/' + file);
                        const pluginName = path.parse(file);
                        pluginsAdd(pluginName.name, plugin);
                        logger.info(`load plugin ${service}/${file}`);
                        result('ok');
                    }
                });
            }
        });
    } catch (error) { 
        logger.info('Error occured while loading plugins', {error});
    }
})};

var pluginsAdd = function(codeEvent, pointerFonction) {
  if (!global.PLUGINS.has(codeEvent)) { global.PLUGINS.set(codeEvent, []) }
  global.PLUGINS.get(codeEvent).push(pointerFonction);
}


var pluginsExecute = function(service, codeEvent, input, request) {
    return new Promise((result, error) => {
        if (global.PLUGINS.has(service)) {
            try {
                const functions = global.PLUGINS.get(service);
                if (functions && functions.length > 0) {
                    const functionToExecute = functions[0][codeEvent];
                    return result(functionToExecute(input, request));
                } else {
                    throw new Error(`No functions found for plugin ${service}`);
                }
            } catch (err) {
                throw new Error(err);
            }
        } else {
            throw new Error(`Plugin ${service} not found`);
        }
    });
}

module.exports = {pluginsLoad, pluginsExecute};