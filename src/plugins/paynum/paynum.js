const axios= require('axios');
const opn = require('opn');
const basicAuth= 'Basic bW9lei5zb21haUBzb2Z0ZWFtLmZyOm5rZWcyNFpaRnQ2R0VnVFQ1dmV6Zw==';
const baseUrl ='https://ppf.payrct.fr/ppf'

async function payByCreditCard(param) {
    try {
        for (const invoice of param.invoices) {
            try {
                const responsePayment = await axios.get(`${baseUrl}/facture/show/${invoice.id}.json`, {
                    headers: {
                        'Authorization': basicAuth
                    }
                });
                await opn(`https://gaia-x.payrct.fr/${responsePayment.data.code}`);
                console.log(`Invoice ${invoice.id} processed successfully.`);
            } catch (error) {
                console.error(`Error processing invoice ${invoice.id}: ${error.message}`);
            }
        }
        return 'Successfully Redirected to PayNums payment interface'
    } catch (error) {
        throw new Error('Error Redirecting to PayNums payment interface: ' + error.message);
    }
}

async function payByDebit(param) {
    try {
        //get creancier Code
        const responsePayment = await axios.get(`${baseUrl}/paieCollecte/save/${param.creditorId}.json`, {
            params: Object.assign(
                { codeBarre: param.invoiceId },
                param.amount !== null ? { montant: param.amount } : {}
            ),
            headers: {
                'Authorization': basicAuth
            }
        });
        await opn(`https://gaia-x.payrct.fr/${responsePayment.data.code}`)
        return 'Successfully Redirected to PayNums payment interface'
    } catch (error) {
        throw new Error('Error Redirecting to PayNums payment interface: ' + error.message);
    }
}

async function creditCard(req) {

        try {
            const response = await axios.post(`${baseUrl}/facture/save.json?debiteur.creancier.code=${req.body.creditorCode}&montant=${req.body.invoice.amount}&callBackURL=${req.body.invoice.callBackURL}&debiteur.email=${req.body.invoice.debitorEmail}&emailDeb=true`, {}, {
                headers: {
                    'Authorization': basicAuth
                }
            });

            return  {
                idExternal: response.data.code, // id of the invoice in the payment solution
                paymentLink: `https://${req.body.creditorCode}.payrct.fr/${response.data.code}`
            };  //invoice
        } catch (error) {
            throw new Error('Error generating Invoice through PayNum plugin: ' + error.message);
        }

}


const checkPluginName = async () => {
        return 'PAYNUM';
};

module.exports = { checkPluginName, payByCreditCard, payByDebit, creditCard };